import {externalSchematic, Rule, SchematicContext, Tree} from '@angular-devkit/schematics';


// You don't have to export the function as default. You can also have more than one rule factory
// per file.
export function createFunctionalModule(_options: any): Rule {
  const DEFAULT_PREFIX: string = 'test-';

  console.log('Create Functional Module: ', _options);

  const {name, routing} = _options;
  if(!name.startsWith(DEFAULT_PREFIX)) {
    throw new Error(`Name must start with ${DEFAULT_PREFIX}`);
  }

  // return externalSchematic('@schematics/angular', 'ng', {
  //   name,
  //   version: '9.0.0',
  //   routing: !!routing,
  //   styles: 'scss'
  // })
  return (tree: Tree, _context: SchematicContext) => {
    return tree;
  };
}
