import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-lib1',
  template: `
    <p>
      lib1 works, HELLO!
    </p>
  `,
  styles: [
  ]
})
export class Lib1Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
